//
//  SCPhotoCell.swift
//  SwiftyCamera
//
//  Created by Camilo Rodriguez Gaviria on 06/06/15.
//  Copyright (c) 2015 Nifflheim. All rights reserved.
//

import Foundation
import UIKit

class SCPhotoCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    //MARK: - Properties
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    func configureWithPhoto(photo: SCPhoto) {
        photoImageView.image = photo.image
        dateLabel.text = SCDateFormatter().stringFromDate(photo.date)
    }
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    //MARK: - <#Delegates#>
}