//
//  SCPhotosViewController.swift
//  SwiftyCamera
//
//  Created by Camilo Rodriguez Gaviria on 06/06/15.
//  Copyright (c) 2015 Nifflheim. All rights reserved.
//

import Foundation
import UIKit

private let SCPhotoCellIdentifier = "SCPhotoCellIdentifier"

class SCPhotosViewController: UIViewController, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    //MARK: - Properties
    
    private var photos = [SCPhoto]()
    private lazy var imagePickerController = UIImagePickerController()
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //MARK: - Actions
    
    @IBAction func takePhotoButtonTapped(sender: UIButton) {
        imagePickerController.delegate = self
        imagePickerController.sourceType = .Camera
        imagePickerController.cameraDevice = .Rear
        imagePickerController.showsCameraControls = true
        
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: - Internal
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    //MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let photo = photos[indexPath.row]
        
        if let cell = cell as? SCPhotoCell {
            cell.configureWithPhoto(photo)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCellWithIdentifier(SCPhotoCellIdentifier, forIndexPath: indexPath) as! SCPhotoCell
    }
    
    //MARK: - UITableViewDelegate
    
    //MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        let photo = SCPhoto(image: image, date: NSDate())
        photos.append(photo)
        tableView.reloadData()
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}