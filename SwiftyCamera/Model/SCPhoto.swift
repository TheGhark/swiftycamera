//
//  SCPhoto.swift
//  SwiftyCamera
//
//  Created by Camilo Rodriguez Gaviria on 06/06/15.
//  Copyright (c) 2015 Nifflheim. All rights reserved.
//

import Foundation
import UIKit

class SCPhoto {
    //MARK: - Properties
    
    var image: UIImage?
    
    var date = NSDate()
    
    //MARK: - Computed Properties
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    
    convenience init(image: UIImage, date: NSDate) {
        self.init()
        self.image = image
        self.date = date
    }
    
    //MARK: - Actions
    
    //MARK: - Internal
    
    //MARK: - Public
    
    //MARK: - Private
    
    //MARK: - Overridden
    
    //MARK: - <#Delegates#>
}