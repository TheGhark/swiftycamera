//
//  SCSettings.swift
//  SwiftyCamera
//
//  Created by Camilo Rodriguez Gaviria on 06/06/15.
//  Copyright (c) 2015 Nifflheim. All rights reserved.
//

import Foundation

private struct SCDateFormatterStruct {
    static var dateFormatter: NSDateFormatter!
    static var onceToken: dispatch_once_t = 0
}

func SCDateFormatter() -> NSDateFormatter {
    dispatch_once(&SCDateFormatterStruct.onceToken, {
        SCDateFormatterStruct.dateFormatter = NSDateFormatter()
        SCDateFormatterStruct.dateFormatter.dateFormat = "EEEE MMM d H:mm a"
    })
    
    return SCDateFormatterStruct.dateFormatter
}